class Album < ActiveRecord::Base
  has_many :tracks
  has_many :playlists, through: :tracks

  def track_count
    self.tracks.count
  end
end
