require 'active_record'
require 'pry'
require 'faker'

require_relative './track'
require_relative './album'
require_relative './playlist'

ActiveRecord::Base.logger = Logger.new(STDOUT)

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  host: 'localhost',
  # These lines will depend on your setup
  username: 'tester',
  password: 'tester',
  # createdb -O tester lhl_web_w7d2_19032019
  database: 'lhl_web_w7d2_19032019'
)

ActiveRecord::Schema.define do

  create_table :tracks, force: true do |t|
    t.string :title, null: false
    t.integer :length, null: false
    t.bigint :album_id # foreign key into albums
  end
  add_index :tracks, :album_id

  create_table :albums, force: true do |t|
    t.string :title, null: false
  end

  create_table :playlists, force: true do |t|
    t.string :name, null: false
  end

  create_join_table :tracks, :playlists, force: true do |t|
    t.index :track_id
    t.index :playlist_id
  end

end

# equivalent of Album.create!(title: "aoeuaoeu")
#
# album = Album.new
# album.title = "aoeuaoeu"
# if !album.save
#   raise "Oh no"
# end

5.times do
  album = Album.create!(title: Faker::Music.album)
  rand(10).times do
    album.tracks.create!(title: Faker::Music::Phish.song,
                         length: rand(10000))
    # Track.create!(title: "", length: 0, album_id: album.id)
  end
end

all_tracks = Track.all
3.times do
  pl = Playlist.create!(name: Faker::Games::Dota.quote)
  10.times do
    pl.tracks << all_tracks.sample
  end
end


binding.pry
